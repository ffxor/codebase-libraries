﻿using System;
using System.Net;
using System.ComponentModel;
using System.Text.RegularExpressions;

namespace CodeBaseLibraries.NetCore.Extensions
{
    public static class TExtensions
    {
        /// <summary>
        /// Retrieves <see cref="DescriptionAttribute"/> value for enum value
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string GetDescription(this Enum value)
        {
            var fi = value.GetType().GetField(value.ToString());
            var attributes = (DescriptionAttribute[])fi.GetCustomAttributes(typeof(DescriptionAttribute), false);
            return (attributes.Length > 0) ? attributes[0].Description : value.ToString();
        }

        /// <summary>
        /// Retrieves <see cref="DisplayNameAttribute"/> value for enum value
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string GetDisplayName(this Enum value)
        {
            var fi = value.GetType().GetField(value.ToString());
            var attributes = (DisplayNameAttribute[])fi.GetCustomAttributes(typeof(DisplayNameAttribute), false);
            return (attributes.Length > 0) ? attributes[0].DisplayName : value.ToString();
        }

    }
}
