﻿using System.Collections.Generic;
using System.Security.Claims;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.OAuth;
using Microsoft.AspNetCore.Http;

namespace CodeBaseLibraries.NetCore.Web.Security.OAuth.Vk
{
    /// <summary>
    /// Defines a set of options used by <see cref="VkAuthenticationHandler"/>.
    /// </summary>
    public class VkAuthenticationOptions : OAuthOptions
    {
        /// <summary>
        /// Used VK API version
        /// See https://vk.com/dev/versions for more information
        /// </summary>
        public string ApiVersion => "5.92";

        public VkAuthenticationOptions()
        {
            ClaimsIssuer = VkAuthenticationDefaults.Issuer;
            CallbackPath = new PathString("/signin-vk");
            

            AuthorizationEndpoint = "https://oauth.vk.com/authorize";
            TokenEndpoint = "https://oauth.vk.com/access_token";
            UserInformationEndpoint = "https://api.vk.com/method/users.get";

            ClaimActions.MapJsonKey(ClaimTypes.NameIdentifier, "id");
            ClaimActions.MapJsonKey(ClaimTypes.GivenName, "first_name");
            ClaimActions.MapJsonKey(ClaimTypes.Surname, "last_name");
        }

        /// <summary>
        /// Gets the list of fields to retrieve from the user information endpoint.
        /// See https://vk.com/dev/objects/user for more information
        /// </summary>
        public ISet<string> Fields { get; } = new HashSet<string>
        {
            "id",
            "first_name",
            "last_name"
        };
    }
}
