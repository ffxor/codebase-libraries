﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration.Ini;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Options;
using Microsoft.AspNetCore.Builder;

namespace CodeBaseLibraries.NetCore.Web.Localization
{
    public class IniStringLocalizer : IStringLocalizer
    {
        protected string localizationPath;
        protected CultureInfo defaultCulture;
        protected IniConfigurationProvider ini;

        public IniStringLocalizer(string LocalizationPath, CultureInfo DefaultCulture)
        {
            this.localizationPath = LocalizationPath;
            this.defaultCulture = DefaultCulture;

            this.ini = new IniConfigurationProvider(new IniConfigurationSource()
            {
                FileProvider = new PhysicalFileProvider(this.localizationPath),
                Path = $"{CultureInfo.CurrentCulture.Name.ToUpper()}.ini",
                Optional = true
            });
            this.ini.Load();
        }

        /// <summary>
        /// Gets the string resource with the given name
        /// </summary>
        /// <param name="name">The name of the string resource</param>
        /// <returns></returns>
        public LocalizedString this[string name]
        {
            get
            {
                var value = this.getString(name);
                return new LocalizedString(name, value ?? name, resourceNotFound: value == null);
            }
        }

        /// <summary>
        /// Gets the string resource with the given name and formatted with the supplied arguments
        /// </summary>
        /// <param name="name">The name of the string resource</param>
        /// <param name="arguments">The values to format the string with</param>
        /// <returns></returns>
        public LocalizedString this[string name, params object[] arguments]
        {
            get
            {
                var format = this.getString(name);
                var value = String.Format(format ?? name, arguments);
                return new LocalizedString(name, value, resourceNotFound: format == null);
            }
        }

        /// <summary>
        /// Gets all string resources
        /// </summary>
        /// <param name="includeParentCultures">A <see cref="Boolean"/> indicating whether to include strings from parent cultures</param>
        /// <returns></returns>
        public IEnumerable<LocalizedString> GetAllStrings(bool includeParentCultures)
            => ini.GetChildKeys(new List<string>(), null).Select(t => new LocalizedString(t, this.getString(t)));

        /// <summary>
        /// Creates a new <see cref="IStringLocalizer"/> for a specific <see cref="CultureInfo"/>
        /// </summary>
        /// <param name="culture">The <see cref="CultureInfo"/> to use</param>
        /// <returns></returns>
        public IStringLocalizer WithCulture(CultureInfo culture)
        {
            //CultureInfo.DefaultThreadCurrentCulture = culture;
            CultureInfo.CurrentCulture = culture;
            return new IniStringLocalizer(this.localizationPath, this.defaultCulture);
        }

        protected string getString(string name)
        {
            ini.TryGet(name, out string value);
            if (value == null && CultureInfo.CurrentCulture != this.defaultCulture)
            {
                var culture = CultureInfo.CurrentCulture;
                value = this.WithCulture(this.defaultCulture)[name];
                CultureInfo.CurrentCulture = culture;
            }
            
            return value;
        }
    }

    public static class LocalizerExtensions
    {
        public static IServiceCollection AddIniStringLocalizer(this IServiceCollection services, string LocalizationPath)
        {
            var localizatiOptions = services.BuildServiceProvider().GetService(typeof(IOptions<RequestLocalizationOptions>)) as IOptions<RequestLocalizationOptions>;
           
            return services
                // DI for Controllers
                .AddTransient<IStringLocalizer, IniStringLocalizer>(t => new IniStringLocalizer(LocalizationPath, localizatiOptions.Value.DefaultRequestCulture.Culture))
                // DI for Views
                .AddSingleton<IStringLocalizerFactory>(new IniStringLocalizerFactory(LocalizationPath, localizatiOptions.Value.DefaultRequestCulture.Culture));
        }
    }
}
