﻿using System;
using System.IO;
using NUnit.Framework;
using Moq;
using CodeBaseLibraries.Serialization;

namespace UnitTests
{
    [TestFixture]
    public class SerializationHelperTests
    {
        /// <summary>
        /// Mock object for Serialize/Deserialize testing
        /// </summary>
        public class MockObject
        {
            public int Id { get; set; }
            public string Name { get; set; }
            public DateTime Date { get; set; }
            public MockObject() { }
        }

        MockObject setup()
        {
            return new MockObject()
            {
                Id = 5,
                Name = "Name",
                Date = DateTime.Now
            };
        }

        [Test]
        public void SerializationHelper_XmlString_Equals()
        {
            // arrange
            MockObject src = setup();

            // act
            var value = XmlSerializationHelper.SerializeToString(src);
            var dest = XmlSerializationHelper.DeserializeFromString<MockObject>(value);

            // assert
            Assert.That(() =>
            {
                return
                    dest.Id.Equals(src.Id) &&
                    dest.Name.Equals(src.Name) &&
                    dest.Date.Equals(src.Date);
            });
        }

        [Test]
        public void SerializationHelper_XmlFile_Equals()
        {
            // arrange
            MockObject src = setup();

            // act
            string filePath = "file.txt";
            XmlSerializationHelper.SerializeToFile(src, filePath);
            var dest = XmlSerializationHelper.DeserializeFromFile<MockObject>(filePath);
            File.Delete(filePath);

            // assert
            Assert.That(() =>
            {
                return
                    dest.Id.Equals(src.Id) &&
                    dest.Name.Equals(src.Name) &&
                    dest.Date.Equals(src.Date);
            });
        }

        [Test]
        public void SerializationHelper_JsonString_Equals()
        {
            // arrange
            MockObject src = setup();

            // act
            var value = JsonSerializationHelper.SerializeToString(src);
            var dest = JsonSerializationHelper.DeserializeFromString<MockObject>(value);

            // assert
            Assert.That(() =>
            {
                return
                    dest.Id.Equals(src.Id) &&
                    dest.Name.Equals(src.Name) &&
                    dest.Date.Date.Equals(src.Date.Date);
            });
        }

        [Test]
        public void SerializationHelper_JsonFile_Equals()
        {
            // arrange
            MockObject src = setup();

            // act
            string filePath = "file.json";
            JsonSerializationHelper.SerializeToFile(src, filePath);
            var dest = JsonSerializationHelper.DeserializeFromFile<MockObject>(filePath);
            File.Delete(filePath);

            // assert
            Assert.That(() =>
            {
                return
                    dest.Id.Equals(src.Id) &&
                    dest.Name.Equals(src.Name) &&
                    dest.Date.Date.Equals(src.Date.Date);
            });
        }
    }
}
