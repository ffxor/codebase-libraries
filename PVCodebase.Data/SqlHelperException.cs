﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PVCodebase.Data
{
    public class SqlHelperException : Exception
    {
        public string QueryText { get; protected set; }
    }
}
