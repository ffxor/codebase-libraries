﻿using System;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace CodeBaseLibraries.Serialization
{
    /// <summary>
    /// XmlSerialization Helper
    /// </summary>
    public class XmlSerializationHelper
    {
        /// <summary>
        /// Generates an instance of object from file with serialized data
        /// </summary>
        /// <typeparam name="T">Type of an instance of object</typeparam>
        /// <param name="Path">Path to file contains serialized data</param>
        /// <returns>An instance of object</returns>
        public static T DeserializeFromFile<T>(string Path)
        {
            T result = default(T);

            using (StreamReader file = new StreamReader(Path))
            {
                result = loadFromStream<T>(file);
            }
            return result;
        }

        /// <summary>
        /// Generates an instance of object from string serialized data
        /// </summary>
        /// <typeparam name="T">Type of an instance of object</typeparam>
        /// <param name="Value">String serialized data</param>
        /// <returns>An instance of object</returns>
        public static T DeserializeFromString<T>(string Value)
        {
            T result = default(T);

            using (StringReader reader = new StringReader(Value))
            {
                result = loadFromStream<T>(reader);
            }
            return result;
        }

        /// <summary>
        /// Saves serialization object data to the file
        /// </summary>
        /// <param name="Instance">Instance of the object</param>
        /// <param name="Path">Path to new file</param>
        public static void SerializeToFile(object Instance, string Path)
        {
            SerializeToFile(Instance, Path, null);
        }

        /// <summary>
        /// Saves serialization object data to the file
        /// </summary>
        /// <param name="Instance">Instance of the object</param>
        /// <param name="Path">Path to new file</param>
        /// <param name="AttributeOverrides">XmlAttributeOverrides object</param>
        public static void SerializeToFile(object Instance, string Path, XmlAttributeOverrides AttributeOverrides)
        {
            using (FileStream fs = new FileStream(Path, FileMode.Create, FileAccess.Write))
            {
                XmlSerializer serializer = new XmlSerializer(Instance.GetType(), AttributeOverrides);
                serializer.Serialize(fs, Instance);
            }
        }

        /// <summary>
        /// Serializes into string object 
        /// </summary>
        /// <param name="Instance">Instance of the object</param>
        /// <returns>String serialized data</returns>
        public static string SerializeToString(object Instance)
        {
            return SerializeToString(Instance, null);
        }

        /// <summary>
        /// Serializes into string object
        /// </summary>
        /// <param name="Instance">Instance of the object</param>
        /// <param name="AttributeOverrides">XmlAttributeOverrides object</param>
        /// <returns>String serialized data</returns>
        public static string SerializeToString(object Instance, XmlAttributeOverrides AttributeOverrides)
        {
            var memory = new MemoryStream();
            var xmlWriter = new XmlTextWriter(memory, new UTF8Encoding(false));
            XmlSerializer serializer = new XmlSerializer(Instance.GetType(), AttributeOverrides);
            serializer.Serialize(xmlWriter, Instance);
            return Encoding.ASCII.GetString(memory.GetBuffer(), 0, (int)memory.Length);
        }

        /// <summary>
        /// Deserializes an instance of object from stream
        /// </summary>
        /// <typeparam name="T">Type of an instance of object</typeparam>
        /// <param name="stream"></param>
        /// <returns>An instance of object</returns>
        private static T loadFromStream<T>(TextReader stream)
        {
            T result = default(T);
            XmlSerializer serializer = new XmlSerializer(typeof(T));

            try
            {
                result = (T)serializer.Deserialize(stream);
            }
            catch (InvalidOperationException ex)
            {
                //InnerException containts data
                throw ex.InnerException;
            }
            finally
            {
                stream.Close();
            }

            return result;
        }
    }
}
