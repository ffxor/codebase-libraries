﻿namespace CodeBaseLibraries.Serialization
{
    public abstract class ASerializationHelper
    {
        public abstract T DeserializeFromString<T>(string Value);
        public abstract T DeserializeFromFile<T>(string Path);

        public abstract string SerializeToString(object Instance);
        public abstract void SerializeToFile(object Instance, string Path);
    }
}
